import Vue from 'vue';
import MainComponent from './components/Main.vue';
import VueRouter from 'vue-router';
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlus, faBars, faMinus, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import App from './components/App.vue';
import * as p5 from 'p5';
import 'p5/lib/addons/p5.sound.min';
import EventBus from 'vue-bus-ts';
// const Kempelen    = () => import( /* webpackChunkName: "kempelen"  */  './components/Kempelen.vue' );
// const Resonator   = () => import( /* webpackChunkName: "resonator" */  './components/Resonator.vue');
// const Formant     = () => import( /* webpackChunkName: "formant"   */  './components/Formant.vue'  );
// const Synthesis   = () => import( /* webpackChunkName: "synthesis" */  './components/Synthesis.vue');
Vue.use(EventBus);
var bus = new EventBus.Bus();
var test = new p5.FFT();
var routes = [
    { path: '/test/', component: App, name: 'test' },
    { path: '*', redirect: '/test' }
];
var router = new VueRouter({
    routes: routes
});
Vue.use(VueRouter);
var v = new Vue({
    el: '#app',
    template: "\n    <div>\n        <main-component></main-component>\n    </div>\n    ",
    components: {
        MainComponent: MainComponent,
    },
    router: router,
    bus: bus
});
// Fontawesome
library.add(faPlus, faBars, faMinus, faTimes);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.config.productionTip = false;
//# sourceMappingURL=index.js.map