var EarconEffect = /** @class */ (function () {
    function EarconEffect(steps, pitches) {
        this.steps = steps || new Array();
        this.pitches = pitches || new Array();
    }
    EarconEffect.prototype.length = function () {
        return this.steps.length;
    };
    EarconEffect.prototype.cutIfTooLong = function (maxSize) {
        if (this.steps.length >= maxSize) {
            this.steps.slice(this.steps.length - maxSize, this.steps.length);
            this.pitches.slice(this.pitches.length - maxSize, this.pitches.length);
        }
    };
    return EarconEffect;
}());
export { EarconEffect };
//# sourceMappingURL=Effect.js.map