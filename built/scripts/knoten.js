import { EarconEffect } from "../models/Effect";
var Knoten = /** @class */ (function () {
    function Knoten(name) {
        this.active = false;
        this.color = "grey";
        this.name = name;
        this.x = 100;
        this.y = 100;
        //this.loop = 1;
        //this.pitch = 0;
        this.children = [];
        this.active = false;
        this.color = "grey";
        this.ancestors = [];
        this.earconEffect = new EarconEffect();
    }
    //Dem Knoten ein Child-Koten hinzufügen
    Knoten.prototype.addChild = function (name) {
        if (name == this.name) {
            name = name + " new";
            var knoten = new Knoten(name);
        }
        else
            var knoten = new Knoten(name);
        this.ancestors.forEach(function (item) {
            knoten.addOrigin(item);
        });
        knoten.addOrigin(this);
        this.children.push(knoten);
        console.log(this.name, " wurde ", name, " hinzugefügt.");
    };
    //gibt zurück, ob der Knoten mindestens 1 Kind hat
    Knoten.prototype.hasChildren = function () {
        if (this.children.length == 0)
            return false;
        return true;
    };
    Knoten.prototype.hasChild = function (knoten) {
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i] == knoten)
                return true;
        }
        return false;
    };
    Knoten.prototype.getAllToRoot = function () {
        var parentKnoten = [];
        parentKnoten.push(this);
        var actKnoten = this;
        while (actKnoten.hasAncestor) {
            actKnoten = actKnoten.ancestors[0];
            parentKnoten.push(actKnoten);
        }
        return parentKnoten;
    };
    //Gibt einen bestimmten Child-Koten zurück
    Knoten.prototype.getchild = function (index) {
        console.log("getchild(index): ");
        console.log(this.children);
        if (this.children.length > 0) {
            return this.children[index];
        }
        return null;
    };
    //Gibt ein Array mit Knoten zurück
    Knoten.prototype.getChildren = function () {
        var kinners = [];
        this.children.forEach(function (item, index, array) {
            kinners.push(item);
        });
        return kinners;
    };
    Knoten.prototype.deleteChild = function (knoten) {
        this.children.splice(this.children.indexOf(knoten), 1);
    };
    Knoten.prototype.addOrigin = function (ancestor) {
        this.ancestors.push(ancestor);
    };
    Knoten.prototype.getPath = function () {
        var path = [];
        this.getAncestors().forEach(function (item) {
            path.push(item);
        });
        path.push(this);
        return path;
    };
    Knoten.prototype.getDirectParent = function () {
        if (this.ancestors.length > 0)
            return this.ancestors[this.ancestors.length - 1];
        else
            return false;
    };
    Knoten.prototype.getAncestors = function () {
        return this.ancestors;
    };
    Knoten.prototype.hasAncestor = function (knoten) {
        for (var i = 0; i < this.ancestors.length; i++) {
            if (this.ancestors[i] == knoten)
                return true;
        }
        return false;
    };
    Knoten.prototype.getx = function () { return this.x; };
    Knoten.prototype.gety = function () { return this.y; };
    Knoten.prototype.setx = function (x) { this.x = x; };
    Knoten.prototype.sety = function (y) { this.y = y; };
    Knoten.prototype.getName = function () { return this.name; };
    Knoten.prototype.setName = function (name) { this.name = name; };
    //Setzt den Knoten auf Aktiv (ausgewählt)
    Knoten.prototype.setActive = function () { this.active = true; };
    //Setzt den Knoten auf Inaktiv (nicht ausgewählt)
    Knoten.prototype.setInactive = function () { this.active = false; };
    //Gibt zurück, ob ein Knoten aktiv ist bzw. ausgewählt/angeklickt wurde
    Knoten.prototype.isActive = function () {
        if (this.active)
            return true;
        else
            return false;
    };
    //gibt die Farbe eines Knotens zurück
    Knoten.prototype.getColor = function () { return this.color; };
    //Ändere die Farbe des Knotens
    Knoten.prototype.setColor = function (color) { this.color = color; };
    return Knoten;
}());
export { Knoten };
//# sourceMappingURL=knoten.js.map