import 'p5/lib/addons/p5.sound.min';
import 'p5/lib/addons/p5.dom.min';
import { Knoten } from './knoten';
import { Logger } from "../scripts/Logger/Logger";
var EarconHandler = /** @class */ (function () {
    function EarconHandler(p5 /*, Knoten*/) {
        this.p5 = p5;
        this.firstEarcon = new Knoten("Project");
        this.tree = [[this.firstEarcon]];
        this.knotsize = 50;
        this.knotlist = [];
        this.clickable = [];
        this.connectionList = [];
        this.listwidth = 150;
        this.activeEarcon = null;
        this.knotNames = [];
        this.entername = false;
        this.typeText = "";
    }
    EarconHandler.prototype.getEntername = function () { return this.entername; };
    EarconHandler.prototype.setEntername = function (bool) { this.entername = bool; };
    EarconHandler.prototype.getTheActiveOne = function () { return this.activeEarcon; };
    EarconHandler.prototype.setTheActiveOne = function (earcon) { this.activeEarcon = earcon; };
    //Gibt ein Array mit Knoten zurück. Anordnung in DeepSearch Reihenfolge.
    EarconHandler.prototype.getKnotenlist = function () {
        Logger.toni("# getKnotenlist #");
        var knotenliste = [];
        var stack = [];
        stack.push([this.tree[0][0]]);
        while (stack.length > 0) {
            if (stack[stack.length - 1][0].hasChildren()) {
                knotenliste.push(stack[stack.length - 1][0]);
                //this.clickable.push(["rect", stack[stack.length-1][0], knotenliste.length-1]);
                stack.push(stack[stack.length - 1][0].getChildren());
                stack[stack.length - 2].shift();
                if (stack[stack.length - 2].length == 0)
                    stack.splice(stack.length - 2, 1);
            }
            else {
                knotenliste.push(stack[stack.length - 1][0]);
                //this.clickable.push(["rect", stack[stack.length-1][0], knotenliste.length-1]);
                stack[stack.length - 1].shift();
                if (stack[stack.length - 1].length == 0)
                    stack.splice(stack.length - 1, 1);
            }
        }
        //this.clickable.push(["+", knotenliste.length]);
        this.knotNames = knotenliste;
        return knotenliste;
    };
    /*drawKnotenlist() {
        var textSeis = this.listwidth*0.1;
        this.p5.textSize(textSeis);
        
        var position = 1;
        this.knotNames.forEach((item: any) => {
                //line(0,30*position-25,breite,30*position-25);
                var color = item.getColor();
                if(item.isActive()) {
                    if(this.entername) {
                        this.p5.stroke("red");
                        this.p5.strokeWeight(3);
                        this.p5.fill("white");
                        this.p5.rect(0,(30*position)-(textSeis+5),this.listwidth,textSeis+12);
                        this.p5.stroke("black");
                        this.p5.fill("black");
                        this.p5.strokeWeight(1);
                        this.p5.text(item.getName(), 10, 30*position);
                    } else {
                        this.p5.stroke("black");
                        this.p5.strokeWeight(1);
                        this.p5.fill("black");
                        this.p5.rect(0,(30*position)-(textSeis+5),this.listwidth,textSeis+12);
                        this.p5.stroke(color);
                        this.p5.fill(color);
                        this.p5.text(item.getName(), 10, 30*position);
                    }
                } else {
                    this.p5.stroke("black");
                    this.p5.fill(color);
                    this.p5.strokeWeight(1);
                    this.p5.rect(0,(30*position)-(textSeis+5),this.listwidth,textSeis+12);
                    
                    //stroke("black");
                    this.p5.noStroke();
                    this.p5.fill("black");
                    this.p5.text(item.getName(), 10, 30*position);
                }
                
                
                //item.setInactive();
                position++;
        });
        this.p5.stroke("black");
        this.p5.fill("grey");
        this.p5.rect(0,(30*position)-(textSeis+5),this.listwidth,textSeis+12);
        this.p5.stroke("black");
        this.p5.fill("grey");
        this.p5.text("+", this.listwidth/2-10, 30*position);
    } */
    EarconHandler.prototype.drawKnoten = function (x, y, active, color, name) {
        //einzelnen Knoten zeichnen
        this.p5.textSize(15);
        if (active) {
            //Text
            this.p5.noStroke();
            this.p5.fill(color);
            this.p5.text(name, x - name.length * 3, y + 43);
            // Earcon
            this.p5.strokeWeight(8);
            this.p5.stroke("white");
            this.p5.fill(color);
        }
        else {
            //Text
            this.p5.noStroke();
            this.p5.fill("white");
            this.p5.text(name, x - name.length * 3, y + 43);
            // Earcon
            this.p5.strokeWeight(4);
            this.p5.stroke(color);
            this.p5.fill("white");
        }
        this.p5.ellipse(x, y, this.knotsize);
        //strokeWeight(4);
    };
    EarconHandler.prototype.drawBackground = function () {
        var _this = this;
        var levelwidth = 30;
        this.tree.forEach(function (item, index) {
            var i = (index + 1);
            _this.p5.noStroke();
            _this.p5.fill("white");
            _this.p5.ellipse(50, i * 100 - 50, levelwidth);
            _this.p5.ellipse(_this.p5.width - 50, i * 100 - 50, levelwidth);
            _this.p5.rect(50, i * 100 - 50 - levelwidth / 2, _this.p5.width - 100, levelwidth);
        });
    };
    EarconHandler.prototype.drawTree = function () {
        var _this = this;
        //Logger.toni("############### test ##########");
        //Logger.toni(this.knotlist);
        this.knotlist.forEach(function (item, index, array) {
            item.forEach(function (jitem, jindex, array) {
                // Die Connections des Knotens malen
                _this.p5.stroke("white");
                _this.p5.strokeWeight(4);
                //Logger.toni("connections[index][jindex]");
                //Logger.toni(index, jindex);
                //Logger.toni(connections[index][jindex]);
                if (index < _this.connectionList.length) {
                    if (jindex < _this.connectionList[index].length) {
                        _this.connectionList[index][jindex].forEach(function (citem, cindex, array) {
                            _this.p5.noFill();
                            _this.p5.line(_this.connectionList[index][jindex][cindex][0], _this.connectionList[index][jindex][cindex][1] + 50, _this.connectionList[index][jindex][cindex][0], _this.connectionList[index][jindex][cindex][1] + 65);
                            _this.p5.bezier(_this.connectionList[index][jindex][cindex][0], _this.connectionList[index][jindex][cindex][1] + 65, _this.connectionList[index][jindex][cindex][2], _this.connectionList[index][jindex][cindex][3] - 35, _this.connectionList[index][jindex][cindex][2], _this.connectionList[index][jindex][cindex][3] - 35, _this.connectionList[index][jindex][cindex][2], _this.connectionList[index][jindex][cindex][3]);
                            /*this.p5.line(this.connectionList[index][jindex][cindex][0],
                                this.connectionList[index][jindex][cindex][1]+65,
                                this.connectionList[index][jindex][cindex][2],
                                this.connectionList[index][jindex][cindex][3]-35);
                            this.p5.line(this.connectionList[index][jindex][cindex][2],
                                this.connectionList[index][jindex][cindex][3]-35,
                                this.connectionList[index][jindex][cindex][2],
                                this.connectionList[index][jindex][cindex][3]);*/
                        });
                    }
                }
                //Die Knoten malen
                //Logger.toni("draw knoten: " , jitem);
                if (jitem[2]) {
                    _this.drawAddButton(index + 2);
                    _this.drawDeleteButton(index + 1);
                }
                _this.drawKnoten(jitem[0], jitem[1], jitem[2], jitem[3], jitem[4]);
            });
        });
        //Logger.toni("nach drawKnoten:   ", this.clickable);
    };
    EarconHandler.prototype.drawAddButton = function (level) {
        var levelwidth = 30;
        this.p5.stroke("white");
        this.p5.strokeWeight(4);
        this.p5.fill("white");
        this.p5.ellipse(this.p5.width - 50, level * 100 - 50, levelwidth + 5);
        this.p5.textSize(20);
        this.p5.noStroke();
        this.p5.fill("gray");
        this.p5.text("+", this.p5.width - 55, level * 100 - 44);
        //this.clickable.splice(this.clickable.length-2, 1);
        this.clickable.push(["+", this.p5.width - 50, level * 100 - 50, levelwidth + 5]);
        //Logger.toni(this.clickable);
        //Logger.toni("############# ^  Plus Button wurde geadded ^ #########");
    };
    EarconHandler.prototype.drawDeleteButton = function (level) {
        var levelwidth = 30;
        this.p5.stroke("white");
        this.p5.strokeWeight(4);
        this.p5.fill("white");
        this.p5.ellipse(this.p5.width - 50, level * 100 - 50, levelwidth + 5);
        this.p5.textSize(30);
        this.p5.fill("gray");
        this.p5.noStroke();
        this.p5.text("-", this.p5.width - 55, level * 100 - 44);
        //this.clickable.splice(this.clickable.length-2, 1);
        this.clickable.push(["-", this.p5.width - 50, level * 100 - 50, levelwidth + 5]);
    };
    EarconHandler.prototype.getMaxLeafCount = function (knoten) {
        var i = knoten.getAncestors().length;
        var maxLeafCount = 0;
        var leafCount = 0;
        for (i + 1; i < this.tree.length; i++) {
            leafCount = 0;
            for (var j = 0; j < this.tree[i].length; j++) {
                if (this.tree[i][j].hasAncestor(knoten))
                    leafCount++;
            }
            if (leafCount > maxLeafCount)
                maxLeafCount = leafCount;
        }
        return maxLeafCount;
    };
    EarconHandler.prototype.reorderTree = function () {
        var _this = this;
        //Logger.toni("reorderTree");
        //Logger.toni(tree);
        var mode = "auto";
        var abstand;
        var longestLevel = 0;
        var firstLevelAbstand = [];
        //Position eines Knotens im Objekt aktualisieren entsprechend des Tree-Arrays
        if (mode == "opt") {
            /*leider noch nicht fertig
            Es soll von oben nach unten der Platz aufgeteilt werden. Dabei wird vertikal
            geschaut, was die maximale anzahl an blättern ist um nicht zu wenig platz zu vergeben*/
            for (var i = 0; i < this.tree.length; i++) {
                if (this.tree[i].length > longestLevel)
                    longestLevel = i;
            }
            for (var i = 0; i < this.tree[1].length; i++) {
                firstLevelAbstand.push(0);
                this.tree[longestLevel].forEach(function (element) {
                    if (element.hasAncestor(_this.tree[1][i]))
                        firstLevelAbstand[i]++;
                });
            }
            Logger.toni("lvl 1 Array:  " + firstLevelAbstand);
            var bla = [[this.tree[0][0], (this.p5.width - 50)]]; //speichert die parents und deren platz
            this.tree[0][0].setx((this.p5.width - 50) / 2);
            this.tree[0][0].sety(100 - 50);
            for (var i = 1; i < this.tree.length; i++) {
                var bli = []; //speichert die parents und deren platz (temp)
                var blu = 0; // speichert den gesamten maxLeaf Platzbedarf dieser Ebene
                abstand = 0;
                for (var j = 0; j < this.tree[i].length; j++) {
                    var maxleaf = this.getMaxLeafCount(this.tree[i][j]);
                    if (maxleaf == 0)
                        maxleaf = 1;
                    blu = blu + maxleaf;
                    Logger.toni("blu " + blu);
                }
                for (var j = 0; j < this.tree[i].length; j++) {
                    for (var k = 0; k < bla.length; k++) {
                        Logger.toni(bla);
                        if (bla[k][0].hasChild(this.tree[i][j])) {
                            var maxleaf = this.getMaxLeafCount(this.tree[i][j]);
                            if (maxleaf == 0)
                                maxleaf = 1; // knoten braucht platz, auch, wenn er keine blätter hat
                            bli.push([this.tree[i][j], ((bla[k][1]) / blu) * maxleaf]);
                        }
                    }
                    Logger.toni("abstand " + abstand);
                    this.tree[i][j].setx(abstand + (bli[bli.length - 1][1] / 2));
                    this.tree[i][j].sety((i + 1) * 100 - 50);
                    abstand = abstand + bli[bli.length - 1][1];
                }
                bla = bli;
            }
        }
        if (mode == "auto") {
            for (var i = 0; i < this.tree.length; i++) {
                abstand = (this.p5.width - 100) / (this.tree[i].length + 1);
                for (var j = 0; j < this.tree[i].length; j++) {
                    this.tree[i][j].setx(abstand * (j + 1));
                    this.tree[i][j].sety((i + 1) * 100 - 50);
                }
            }
        }
    };
    EarconHandler.prototype.boot = function () {
        // Hilfsklasse zum erstellen von Demodaten
        // Kann später zum laden von Jsons genutzt werden.
        Logger.toni("=========>boot()");
        if (this.firstEarcon.hasChildren()) { }
        else {
            this.firstEarcon.addChild("Anruf");
            //Logger.toni(firstEarcon);
            //Logger.toni(firstEarcon.getchild(0));
            var meh = this.firstEarcon.getchild(0);
            meh.addChild("Abnehmen");
            meh.addChild("Auflegen");
            meh.getchild(1).setColor("deeppink");
            meh.addChild("Klingeln");
            var muh = meh.getchild(0);
            muh.addChild("neuer Knoten 1");
            this.firstEarcon.addChild("Kontakte");
            this.firstEarcon.getchild(1).addChild("Blubb");
            this.firstEarcon.getchild(1).setColor("aquamarine");
        }
        Logger.toni("<=========boot()");
    };
    EarconHandler.prototype.updateTree = function () {
        var _this = this;
        /*Aufbau eines Tree Arrays. Braucht eine [i][j] Liste mit dem ersten
        Knoten. also Quasi
                var ersterKnoten = new Knoten("erster");
                updateTree([[ersterKnoten]]);    */
        if (this.tree[0][0].getChildren().length == 0)
            return;
        var newline = this.tree[0];
        this.tree = [];
        this.tree.push(newline);
        newline = newline[0].getChildren();
        this.tree.push(newline);
        for (var i = 1; i < this.tree.length; i++) {
            for (var j = 0; j < this.tree[i].length; j++) {
                //Logger.toni("i = " + i + "  j = " + j);
                newline = this.tree[i][j].getChildren();
                if (newline.length != 0) {
                    if (this.tree.length <= i + 1)
                        this.tree.push(newline);
                    else {
                        newline.forEach(function (item, index, array) {
                            _this.tree[i + 1].push(item);
                        });
                    }
                }
            }
        }
        if (this.tree[this.tree.length - 1].length == 0)
            this.tree.pop();
        this.reorderTree();
        this.getKnoten();
        this.getConnections();
        this.getKnotenlist();
        //Logger.toni("Tree", this.tree);
        return this.tree;
    };
    EarconHandler.prototype.getKnoten = function () {
        var _this = this;
        Logger.toni("# getKnoten #");
        //Logger.toni(this.clickable);
        /*  Code um Tree in eine Liste von Knoten-Koordinaten
        umzuwandeln. Um CPU zu schonen und den cache nicht jedes mal vollzumüllen */
        var knoten = [];
        this.tree.forEach(function (item, index, array) {
            knoten.push([]);
            item.forEach(function (jitem, jindex, array) {
                knoten[index].push([jitem.getx(), jitem.gety(), jitem.isActive(), jitem.getColor(), jitem.getName()]);
                _this.clickable.push(["circle", jitem, jitem.getx(), jitem.gety()]);
                //Logger.toni("Knoten: " , jitem);
                //Logger.toni("seine Farbe:  ", jitem.getColor());
            });
        });
        //Logger.toni("getKnoten: ");
        //Logger.toni(knoten);
        this.knotlist = knoten;
        //Logger.toni(this.clickable);
        return knoten;
    };
    EarconHandler.prototype.getConnections = function () {
        /*code um den tree in eine Tree-Array mit Koordinaten umzuwandeln
        um knoten per linie verbinden zu können.*/
        var verbindungen = [];
        var children = [];
        //Logger.toni("===> getConnections() ", verbindungen, children);
        this.tree.forEach(function (item, index, array) {
            verbindungen.push([]);
            item.forEach(function (jitem, jindex, array) {
                //Logger.toni(index, jindex);
                if (jitem.getChildren().length == 0) {
                    return;
                }
                //Logger.toni( "darf nicht 0 sein: ");
                //Logger.toni(jitem.getChildren().length);
                children = jitem.getChildren();
                verbindungen[index].push([]);
                //Logger.toni("tree : ", tree);
                //Logger.toni("children of jitem: ", children);
                //Logger.toni("connection : ", verbindungen);
                //Logger.toni("connections of   ", index,jindex);
                //Logger.toni(verbindungen[index][jindex]);
                children.forEach(function (citem, cindex, array) {
                    verbindungen[index][verbindungen[index].length - 1].push([jitem.getx(), jitem.gety(), citem.getx(), citem.gety()]);
                });
            });
            if (verbindungen[verbindungen.length - 1].length == 0)
                verbindungen.pop();
        });
        //Logger.toni("getConnections: ");
        //Logger.toni(verbindungen);
        this.connectionList = verbindungen;
        return verbindungen;
    };
    EarconHandler.prototype.whatchyaClickin = function (MouseX, MouseY) {
        var _this = this;
        Logger.toni("# whatchyaClickin #");
        var target = null;
        Logger.toni(this.clickable);
        //Logger.toni("MouseX = ", MouseX, "MouseY = ", MouseY);
        this.clickable.forEach(function (item, index, array) {
            if (MouseX > 0 && MouseY > 0) {
                Logger.toni("Maus über der Baumstruktur");
                _this.entername == false;
                if (item[0] == "circle") {
                    if (_this.p5.dist(MouseX, MouseY, item[2], item[3]) <= _this.knotsize) {
                        //Logger.toni("item in der Baumstruktur: ", item[1].getName());
                        target = item[1];
                        return item[1];
                    }
                }
                if (item[0] == "+") {
                    if (_this.p5.dist(MouseX, MouseY, item[1], item[2]) <= item[3]) {
                        Logger.toni("+ wirklich getroffen");
                        if (_this.activeEarcon == null) { }
                        else {
                            _this.entername = true;
                            _this.activeEarcon.addChild("");
                            target = _this.activeEarcon.getchild(_this.activeEarcon.getChildren().length - 1);
                            return target;
                        }
                    }
                }
                if (item[0] == "-") {
                    if (_this.p5.dist(MouseX, MouseY, item[1], item[2]) <= item[3]) {
                        Logger.toni("- wirklich getroffen");
                        if (_this.activeEarcon == null) { }
                        else {
                            if (_this.activeEarcon.getDirectParent() == false) { }
                            else {
                                target = _this.activeEarcon.getDirectParent();
                                _this.activeEarcon.getDirectParent().deleteChild(_this.activeEarcon);
                                return target;
                            }
                        }
                    }
                }
            }
            else {
                _this.entername == false;
                Logger.toni("nichts getroffen");
            }
        });
        this.knotNames.forEach(function (knoten) {
            knoten.setInactive();
        });
        this.clickable = [];
        return target;
    };
    return EarconHandler;
}());
export { EarconHandler };
//# sourceMappingURL=earconHandler.js.map