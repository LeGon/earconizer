import 'p5/lib/addons/p5.sound.min';
import 'p5/lib/addons/p5.dom.min';
var LoadedEffect = /** @class */ (function () {
    function LoadedEffect(start, length, effect) {
        this.startCol = start;
        this.endCol = this.startCol + length;
        this.effect = effect;
    }
    return LoadedEffect;
}());
export { LoadedEffect };
//# sourceMappingURL=LoadedEffect.js.map