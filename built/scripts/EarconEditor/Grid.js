import { GridPoint } from './GridPoint';
import { EarconEditor } from './EarconEditor';
import { Logger } from '../Logger/Logger';
var Grid = /** @class */ (function () {
    function Grid(p5, columns, rows, width, height) {
        this.gridPoints = new Array();
        if (rows % 2 == 0) {
            throw new Error("Rows muss ungerade sein!");
        }
        this.columns = columns;
        this.rows = rows;
        this.height = height;
        this.width = width;
        this.p5 = p5;
        this.createPoints();
    }
    Grid.prototype.createPoints = function () {
        var offset = GridPoint.radius * GridPoint.stroke;
        for (var j = 0; j < this.height; j += this.height / this.rows) {
            for (var i = 0; i < this.width; i += this.width / this.columns) {
                this.gridPoints.push(new GridPoint(this.p5, i + offset, j + offset));
                console.log("point created at " + i + " :: " + j);
            }
        }
    };
    Grid.prototype.getPointAt = function (col, row) {
        return this.gridPoints[row * this.columns + col];
    };
    Grid.prototype.getAllPointsInColumn = function (col) {
        var _this = this;
        return this.gridPoints.filter(function (point, index, gridPoints) {
            return ((index) % (_this.columns) == col);
        });
    };
    Grid.prototype.getColumnOfPoint = function (point) {
        return this.gridPoints.indexOf(point) % this.columns;
    };
    Grid.prototype.getLastActiveStep = function (start, end) {
        var startCol = start || 0;
        var endCol = end || this.columns - 1;
        for (var i = endCol; i >= startCol; i--) {
            var allPointsinCol = this.getAllPointsInColumn(i);
            for (var j = 0; j < allPointsinCol.length; j++) {
                if (allPointsinCol[j].active == true)
                    return i;
            }
        }
        return 0;
    };
    Grid.prototype.findClickedCircle = function (mouseX, mouseY) {
        var _this = this;
        var clickedPoint;
        this.gridPoints.forEach(function (point) {
            if (_this.p5.dist(point.x, point.y, mouseX, mouseY) <= GridPoint.radius)
                clickedPoint = point;
        });
        if (clickedPoint != undefined) {
            this.getAllPointsInColumn(this.getColumnOfPoint(clickedPoint)).filter(function (point) { return point != clickedPoint; }).forEach(function (point) { return point.deactivate(); });
            clickedPoint.clicked();
        }
    };
    Grid.prototype.drawPoints = function () {
        this.gridPoints.forEach(function (point) {
            point.draw();
        });
    };
    Grid.prototype.clear = function () {
        Logger.felix("Grid cleared");
        this.gridPoints.forEach(function (it) { return it.deactivate(); });
    };
    Grid.prototype.drawGrid = function () {
        var p5 = this.p5;
        var lineColor = EarconEditor.primaryColor.makeP5Color(p5);
        var lineStroke = 3;
        var offset = GridPoint.radius * GridPoint.stroke;
        p5.translate(offset, offset);
        p5.stroke(lineColor);
        p5.strokeWeight(lineStroke);
        var colsDrawn = 0;
        for (var i = 0; i < this.width; i += this.width / this.columns) {
            if (!(colsDrawn == 0) && !(colsDrawn == this.columns - 1))
                p5.line(i, 0, i, this.height - this.height / this.rows);
            colsDrawn += 1;
        }
        for (var j = 0; j < this.height; j += this.height / this.rows) {
            p5.line(0, j, this.width - this.width / this.columns, j);
        }
        p5.translate(-offset, -offset);
    };
    return Grid;
}());
export { Grid };
//# sourceMappingURL=Grid.js.map