import * as P5 from 'p5';
import 'p5/lib/addons/p5.sound.min';
import 'p5/lib/addons/p5.dom.min';
import { Grid } from './Grid';
import { MyColor } from './../MyColor';
import { EarconEffectProcessor } from "../EarconEffects/EarconEffectProcessor";
import { LoadedEffect } from './LoadedEffect';
import { Logger } from "../Logger/Logger";
var EarconEditor = /** @class */ (function () {
    function EarconEditor(width, height) {
        this.possibleSequenceSteps = 8;
        this.destroyP5 = false;
        this.width = width;
        this.height = height;
        this.earconEffectProcessor = new EarconEffectProcessor(this.possibleSequenceSteps);
        this.loadedEffects = [];
    }
    EarconEditor.prototype.start = function (ref) {
        new P5(this.sketch.bind(this), ref);
    };
    EarconEditor.prototype.destroy = function () {
    };
    EarconEditor.prototype.exportLoadedEffects = function () {
        var _this = this;
        var loadedEffect = this.loadedEffects;
        loadedEffect.forEach(function (it) { return _this.exportLoadedEffect(it); });
    };
    EarconEditor.prototype.exportLoadedEffect = function (loadedEffect) {
        if (this.grid != undefined) {
            var effect_1 = loadedEffect.effect;
            var grid = this.grid;
            var gridPoints = grid.gridPoints;
            var middlePoint_1 = (grid.rows - 1) / 2;
            var endPoint = grid.getLastActiveStep(loadedEffect.startCol, loadedEffect.endCol);
            var _loop_1 = function (col) {
                effect_1.steps[col] = false;
                grid.getAllPointsInColumn(col).forEach(function (point, index, array) {
                    if (point.active == true) {
                        effect_1.steps[col] = true;
                        effect_1.pitches[col] = index - middlePoint_1;
                    }
                });
            };
            for (var col = loadedEffect.startCol; col <= endPoint; col++) {
                _loop_1(col);
            }
            console.log(effect_1);
            return effect_1; //Hier wird ein neuer EarconEffect erstellt
        }
        else {
            throw Error("Kein Grid vorhanden");
        }
    };
    /*
        exportEffect(): EarconEffect {
            if (this.grid != undefined) {
                let effect = new EarconEffect()
                let grid = this.grid
                let gridPoints = grid.gridPoints
                let middlePoint = (grid.rows - 1) / 2
    
    
                for (let col = 0; col <= grid.getLastActiveStep(); col++) {
                    effect.steps[col] = false
                    grid.getAllPointsInColumn(col).forEach((point, index, array) => {
                        if (point.active == true) {
                            effect.steps[col] = true
                            effect.pitches[col] = index - middlePoint
                        }
                    })
                }
                console.log(effect)
                return effect //Hier wird ein neuer EarconEffect erstellt
            }
            else {
                throw Error("Kein Grid vorhanden")
            }
        }
    
     */
    EarconEditor.prototype.importEffect = function (loadedEffect, effectGroup) {
        if (this.grid != undefined) {
            var grid = this.grid;
            var rows = grid.rows;
            var middlePoint = (rows - 1) / 2;
            var steps = loadedEffect.effect.steps;
            var pitches = loadedEffect.effect.pitches;
            var _loop_2 = function (step) {
                if (steps[step]) {
                    var row_1 = pitches[step] + middlePoint;
                    grid.getAllPointsInColumn(step).forEach(function (point, index, array) {
                        if (index == row_1)
                            point.active = true;
                        point.effectGroup = effectGroup;
                        point.setGroupColor();
                    });
                }
            };
            for (var step = loadedEffect.startCol; step <= loadedEffect.endCol; step++) {
                _loop_2(step);
            }
        }
    };
    /*
        importEffects(effects: EarconEffect[]){
    
            let allEffectSteps: boolean[] = []
            let allEffectPitches: number[] = []
            effects.forEach((it) => {
                for (let i = it.steps.length - 1; i >= 0; i--){
                    allEffectSteps.push(it.steps[i])
                    allEffectPitches.push(it.pitches[i])
                }
            })
            let allInOneEffect = new EarconEffect(allEffectSteps, allEffectPitches)
            allInOneEffect.cutIfTooLong(this.possibleSequenceSteps)
            //this.importEffect(allInOneEffect)
            //this.
        } */
    EarconEditor.prototype.importEffects = function (effects) {
        if (this.grid != undefined) {
            this.grid.clear();
            Logger.felix("importEffects");
            var length_1 = 0;
            var buffer = 3; //die steps die nach recht frei bleiben sollen
            var effectsToShow = []; //die Effekt die in den editor passen
            var loadedEffects = this.loadedEffects; //die geladenden Effekt werden gewrappt um sie einfacher zu bearbeiten
            loadedEffects.length = 0; // array clearen
            effects = effects.filter(function (it) { return it.steps.length > 0 || it == effects[0]; }); // alle leeren Effekte werden ausgenommen außer der des ausgewählten knoten
            Logger.felix(effects);
            var i = effects.length - 1;
            while (length_1 < this.possibleSequenceSteps + buffer && i >= 0) {
                Logger.felix("effect " + effects[i]);
                effectsToShow.push(effects[i]);
                length_1 += effects[i].steps.length - 1;
                i--;
            }
            // effectsToShow.reverse()            //der gewählte Effekt ist jetzt ganz Hinten
            Logger.felix(effectsToShow);
            var newStart = 0; // der nächste effekt soll im grid da dargestellt werden wo der vorherige aufhört
            for (var i_1 = 0; i_1 < effectsToShow.length; i_1++) {
                var thisEffect = effectsToShow[i_1];
                var endPoint = thisEffect.steps.length - 1;
                if (i_1 == effectsToShow.length - 1) {
                    endPoint = this.possibleSequenceSteps - 1;
                }
                var loadedEffect = new LoadedEffect(newStart, endPoint, thisEffect);
                loadedEffects.push(loadedEffect);
                newStart += thisEffect.steps.length;
                this.importEffect(loadedEffect, i_1);
                Logger.felix(loadedEffect);
            }
        }
    };
    EarconEditor.prototype.loadKnoten = function (knoten) {
        var path = knoten.getPath(); //Alle Knoten bis zum ROot
        Logger.felix(path);
        var allEffects = [];
        var allEffectsLength = 0;
        this.activeKnoten = knoten;
        Logger.felix("loadKnoten");
        path.forEach(function (it) {
            allEffects.push(it.earconEffect);
        });
        // allEffects.reverse() //rootknoten wieder am anfang
        this.importEffects(allEffects);
        //allEffects.forEach((it) => { allEffectsLength += it.length() })
    };
    EarconEditor.prototype.sketch = function (p5) {
        var _this = this;
        var backgroundColor = EarconEditor.editorBackgroundColor.makeP5Color(p5);
        var canvasWidth = this.width;
        var canvasHeight = this.height;
        var rows = 7; // muss ungerade sein
        this.grid = new Grid(p5, this.possibleSequenceSteps, rows, canvasWidth, canvasHeight);
        var grid = this.grid;
        //this.importEffect(new EarconEffect(new Array(true, true), new Array(1, -1)))
        p5.setup = function () {
            p5.createCanvas(canvasWidth, canvasHeight);
            p5.background(backgroundColor);
            p5.frameRate(60);
            grid.drawGrid();
        };
        p5.draw = function () {
            if (_this.destroyP5) {
                p5.remove();
            }
            p5.background(backgroundColor);
            grid.drawGrid();
            grid.drawPoints();
        };
        p5.mousePressed = function () {
            if (_this.activeKnoten != undefined) {
                grid.findClickedCircle(p5.mouseX, p5.mouseY);
                _this.exportLoadedEffects();
                Logger.felix(_this.activeKnoten.earconEffect);
            }
        };
    };
    EarconEditor.editorBackgroundColor = new MyColor(0);
    EarconEditor.primaryColor = new MyColor(218);
    EarconEditor.bobbelColor = new MyColor(255, 112, 67, 0.7);
    return EarconEditor;
}());
export { EarconEditor };
//# sourceMappingURL=EarconEditor.js.map