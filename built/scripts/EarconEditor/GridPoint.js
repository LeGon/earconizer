import { EarconEditor } from './EarconEditor';
import { MyColor } from '../MyColor';
var GridPoint = /** @class */ (function () {
    function GridPoint(p5, x, y) {
        this.effectGroup = 0; //Alle Steps aus einem Effect eines sind in einer effectGroup
        this.active = false;
        this.visible = true;
        this.p5 = p5;
        this.x = x;
        this.y = y;
        this.color = EarconEditor.bobbelColor.makeP5Color(p5);
    }
    GridPoint.prototype.draw = function () {
        var p5 = this.p5;
        var backgroundColor = EarconEditor.bobbelColor.makeP5Color(p5);
        var color = this.color;
        if (this.active) {
            color = this.color;
            p5.stroke(color);
            p5.strokeWeight(GridPoint.stroke);
            p5.fill(backgroundColor);
            p5.ellipse(this.x, this.y, GridPoint.radius * 2);
        }
    };
    GridPoint.prototype.clicked = function () {
        this.active = !this.active;
    };
    GridPoint.prototype.deactivate = function () {
        this.active = false;
    };
    GridPoint.prototype.setGroupColor = function () {
        switch (this.effectGroup) {
            case 0: this.color = new MyColor(23, 123, 24, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
            case 1: this.color = new MyColor(123, 12, 24, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
            case 2: this.color = new MyColor(233, 0, 24, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
            case 3: this.color = new MyColor(0, 123, 24, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
            case 4: this.color = new MyColor(2, 1, 240, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
            case 5: this.color = new MyColor(233, 123, 122, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
            case 6: this.color = new MyColor(234, 12, 222, EarconEditor.bobbelColor.alpha).makeP5Color(this.p5);
        }
    };
    GridPoint.radius = 12;
    GridPoint.stroke = 2;
    return GridPoint;
}());
export { GridPoint };
//# sourceMappingURL=GridPoint.js.map