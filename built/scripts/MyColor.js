var MyColor = /** @class */ (function () {
    function MyColor(r, g, b, a) {
        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._alpha = 1;
        this.r = r;
        this.b = b || r;
        this.g = g || r;
        this.alpha = a || 1;
    }
    Object.defineProperty(MyColor.prototype, "r", {
        set: function (r) {
            if (this.isInRange(r))
                this._r = r;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyColor.prototype, "b", {
        set: function (b) {
            if (this.isInRange(b))
                this._b = b;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyColor.prototype, "g", {
        set: function (g) {
            if (this.isInRange(g))
                this._g = g;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyColor.prototype, "alpha", {
        set: function (alpha) {
            if (this.isInRange(alpha), 0, 1)
                this._alpha = alpha;
        },
        enumerable: true,
        configurable: true
    });
    MyColor.prototype.isInRange = function (input, start, stop) {
        start = start || 0;
        stop = stop || 255;
        if (input < start || input > stop)
            return false;
        else
            return true;
    };
    MyColor.prototype.makeP5Color = function (p5) {
        return p5.color('rgba(' + this._r + ',' + this._g + ',' + this._b + ',' + this._alpha + ')');
    };
    return MyColor;
}());
export { MyColor };
//# sourceMappingURL=MyColor.js.map