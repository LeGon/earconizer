var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.felix = function (text) {
        if (this.showFelix)
            console.log(text);
    };
    Logger.toni = function (text) {
        if (this.showToni)
            console.log(text);
    };
    Logger.max = function (text) {
        if (this.showMax)
            console.log(text);
    };
    Logger.showFelix = true;
    Logger.showToni = false;
    Logger.showMax = false;
    return Logger;
}());
export { Logger };
//# sourceMappingURL=Logger.js.map