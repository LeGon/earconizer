import * as Tone from 'tone';
var EarconEffectProcessor = /** @class */ (function () {
    function EarconEffectProcessor(maxSequenceSteps) {
        this.maxSequenceSteps = maxSequenceSteps;
        this.sequenceLoop = new Tone.Loop(this.loopFunction, "4n");
        this.sequenceLoop.iterations = 0;
    }
    EarconEffectProcessor.prototype.applyEffect = function () {
    };
    EarconEffectProcessor.prototype.playEarcon = function (knoten) {
        var knotenToRoot = knoten.getAllToRoot(); // wird nicht funzen
        var allEffects = [];
        knotenToRoot.forEach(function (it) {
            allEffects.push(it.earconEffect);
        });
        var allEffectsLength = 0;
        allEffects.forEach(function (it) { allEffectsLength += it.length(); });
        this.sequenceLoop.iterations = allEffectsLength;
        this.sequenceLoop.start(0);
    };
    EarconEffectProcessor.prototype.loopFunction = function (time) {
    };
    return EarconEffectProcessor;
}());
export { EarconEffectProcessor };
//# sourceMappingURL=EarconEffectProcessor.js.map