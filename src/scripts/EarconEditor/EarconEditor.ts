import * as P5 from 'p5';
import 'p5/lib/addons/p5.sound.min';
import 'p5/lib/addons/p5.dom.min';
import { Grid } from './Grid'
import { EarconEffect } from './../../models/Effect'
import { GridPoint } from './GridPoint';
import { MyColor } from './../MyColor';
import { Knoten } from "../knoten";
import { EarconEffectProcessor } from "../EarconEffects/EarconEffectProcessor";
import { LoadedEffect } from './LoadedEffect';
import { Logger } from "../Logger/Logger";




class EarconEditor {
    static editorBackgroundColor = new MyColor(233)
    static primaryColor: MyColor = new MyColor(62)
    static activeBobbelColor: MyColor = new MyColor(255, 112, 67, 0.7)
    static bobbelColor: MyColor = new MyColor(62, 62,62,0.7)




    possibleSequenceSteps = 8
    destroyP5 = false;
    grid: Grid | undefined
    width: number;
    height: number;
    earconEffectProcessor: EarconEffectProcessor
    activeKnoten: Knoten | undefined
    loadedEffects: LoadedEffect[]

    constructor(width: number, height: number, stopFunction: () => any, startFunction: () => any) {
        this.width = width;
        this.height = height
        this.earconEffectProcessor = new EarconEffectProcessor(this.possibleSequenceSteps, stopFunction, startFunction)
        this.earconEffectProcessor.loadSound("./tone1.mp3")
        this.loadedEffects = []
    }

    start(ref: HTMLElement) {
        new P5(this.sketch.bind(this), ref)

    }

    destroy() {

    }

    exportLoadedEffects() {
        this.loadedEffects.forEach((it) => this.exportLoadedEffect(it))
    }
    exportLoadedEffect(loadedEffect: LoadedEffect) {
        //Logger.felix("laoded effect: " + JSON.stringify(loadedEffect))
        if (this.grid != undefined) {
            let effect = loadedEffect.effect
            let grid = this.grid
            let middlePoint = (grid.rows - 1) / 2
            Logger.felix("exporting effect with start point", loadedEffect.startCol, " and endpoint", loadedEffect.endCol)

            let endPoint = grid.getLastActiveStep(loadedEffect.startCol, loadedEffect.endCol)

            Logger.felix("end point: ", endPoint)
            for (let col = loadedEffect.startCol; col <= endPoint; col++) {
                Logger.felix("durclauf", col)
                let relativeCol = col - loadedEffect.startCol
                effect.steps[relativeCol] = false
                effect.pitches[relativeCol] = 0
                grid.getAllPointsInColumn(col).forEach((point, index, array) => {
                    if (point.active == true) {
                        effect.steps[relativeCol] = true
                        effect.pitches[relativeCol] = index - middlePoint
                    }
                })
            }
            effect.pitches.length = endPoint - loadedEffect.startCol + 1 // damit die arrays wieder gestutzt werden falls sie mal länger waren und steps rausgenommen wurden
            effect.steps.length = endPoint - loadedEffect.startCol + 1
            console.log("exportet effect" + JSON.stringify(effect))
        }
        else {
            throw Error("Kein Grid vorhanden")
        }
    }
    /* 
        exportEffect(): EarconEffect {
            if (this.grid != undefined) {
                let effect = new EarconEffect()
                let grid = this.grid
                let gridPoints = grid.gridPoints
                let middlePoint = (grid.rows - 1) / 2
    
    
                for (let col = 0; col <= grid.getLastActiveStep(); col++) {
                    effect.steps[col] = false
                    grid.getAllPointsInColumn(col).forEach((point, index, array) => {
                        if (point.active == true) {
                            effect.steps[col] = true
                            effect.pitches[col] = index - middlePoint
                        }
                    })
                }
                console.log(effect)
                return effect //Hier wird ein neuer EarconEffect erstellt
            }
            else {
                throw Error("Kein Grid vorhanden")
            }
        }
    
     */

    importEffect(loadedEffect: LoadedEffect, isActiveEffect: boolean) { //effectGroup benutzen um die Farben unterschiedlich zu machen
        if (this.grid != undefined) {
            let grid = this.grid
            let rows = grid.rows
            let middlePoint = (rows - 1) / 2
            let steps = loadedEffect.effect.steps
            let pitches = loadedEffect.effect.pitches
            Logger.felix("drawing effect from", loadedEffect.startCol, "to ", loadedEffect.endCol)
            for (let step = 0; step <= loadedEffect.endCol - loadedEffect.startCol; step++) {
                grid.getAllPointsInColumn(step + loadedEffect.startCol).forEach((point, index, array) => {
                    point.locked = !isActiveEffect //wenn der point zuumaktiven effekt gehört, soll er nicht locked sein
                    point.setGroupColor()
                    if (steps[step]) {
                        let row = pitches[step] + middlePoint
                        if (index == row) {
                            point.active = true
                        }
                    }
                })
            }
        }
    }


    /* 
        importEffects(effects: EarconEffect[]){
    
            let allEffectSteps: boolean[] = []
            let allEffectPitches: number[] = []
            effects.forEach((it) => {
                for (let i = it.steps.length - 1; i >= 0; i--){
                    allEffectSteps.push(it.steps[i])
                    allEffectPitches.push(it.pitches[i])
                }
            })
            let allInOneEffect = new EarconEffect(allEffectSteps, allEffectPitches)
            allInOneEffect.cutIfTooLong(this.possibleSequenceSteps)
            //this.importEffect(allInOneEffect)
            //this.
        } */


    importEffects(effects: EarconEffect[]) {
        if (this.grid != undefined) {
            this.grid.clear();
            let length = 0
            const buffer = 2 //die steps die nach recht frei bleiben sollen
            let effectsToShow: EarconEffect[] = [] //die Effekt die in den editor passen
            let loadedEffects: LoadedEffect[] = this.loadedEffects //die geladenden Effekt werden gewrappt um sie einfacher zu bearbeiten
            loadedEffects.length = 0 // array clearen
            effects = effects.filter((it) => it.steps.length > 0 || it == effects[effects.length - 1]) // alle leeren Effekte werden ausgenommen außer der des ausgewählten knoten
            Logger.felix("importierte effekte: " + JSON.stringify(effects))


            let i = effects.length - 1
            while (length < this.possibleSequenceSteps + buffer && i >= 0) {
                effectsToShow.push(effects[i])
                length += effects[i].steps.length - 1
                i--
            }
            effectsToShow.reverse()            //der gewählte Effekt ist jetzt ganz Hinten

            //Logger.felix(effectsToShow)
            let newStart = 0 // der nächste effekt soll im grid da dargestellt werden wo der vorherige aufhört
            for (let i = 0; i < effectsToShow.length; i++) {
                let isActiveEffect = false //ist es der ausgewählt effekt
                let thisEffect = effectsToShow[i]
                let endPoint = thisEffect.steps.length - 1
                if (i == effectsToShow.length - 1) {
                    endPoint = this.possibleSequenceSteps - 1 // der letzte effekt mus verlängerbar sein
                    isActiveEffect = true
                }
                let loadedEffect = new LoadedEffect(newStart, endPoint, thisEffect)
                loadedEffects.push(loadedEffect)
                newStart += thisEffect.steps.length
                this.importEffect(loadedEffect, isActiveEffect)
                Logger.felix("loaded effect: ", JSON.stringify(loadedEffect))
            }
        }
    }

    get Effects(){
        return this.loadedEffects.map((it) => it.effect);
    }


    loadKnoten(knoten: Knoten) {
        let path = knoten.getPath(); //Alle Knoten bis zum ROot
        Logger.max(path)
        let allEffects: EarconEffect[] = []
        let allEffectsLength = 0;
        this.activeKnoten = knoten

        path.forEach((it) => {
            allEffects.push(it.earconEffect)
            Logger.max("importierte effekte von knoten " + it.name + JSON.stringify(it.earconEffect))
        })
        // allEffects.reverse() //rootknoten wieder am anfang
        this.importEffects(allEffects)
        Logger.max("HIER")
        this.earconEffectProcessor.loadEarcon(this.Effects)
        //allEffects.forEach((it) => { allEffectsLength += it.length() })
    }

    sketch(p5: P5) {
        const backgroundColor = EarconEditor.editorBackgroundColor.makeP5Color(p5)
        const canvasWidth = this.width;
        const canvasHeight = this.height;
        const rows = 7 // muss ungerade sein

        this.grid = new Grid(p5, this.possibleSequenceSteps, rows, canvasWidth, canvasHeight)
        let grid = this.grid
        //this.importEffect(new EarconEffect(new Array(true, true), new Array(1, -1)))
        p5.setup = () => {
            p5.createCanvas(canvasWidth, canvasHeight);
            p5.background(backgroundColor);
            p5.frameRate(60);
            grid.drawGrid()
        }

        p5.draw = () => {
            if (this.destroyP5) {
                p5.remove();
            }
            p5.background(backgroundColor)
            grid.drawGrid()
            grid.drawPoints()
        }

        p5.mousePressed = () => {
            if (this.activeKnoten != undefined) {
                let clickedCircle = grid.findClickedCircle(p5.mouseX, p5.mouseY)
                if (clickedCircle) {
                    let activeStartCol = this.loadedEffects[this.loadedEffects.length - 1].startCol
                    let activeEndCol = this.loadedEffects[this.loadedEffects.length - 1].endCol
                    let clickedColumn = grid.getColumnOfPoint(clickedCircle)
                    Logger.felix("start ", activeStartCol, "end ", activeEndCol, " actual", clickedColumn)
                    if(activeStartCol <= clickedColumn && activeEndCol >= clickedColumn) grid.clickGridPoint(clickedCircle)
                    this.exportLoadedEffects()
                    this.earconEffectProcessor.loadEarcon(this.loadedEffects.map((it) => it.effect))
                }
            }
        }

    }
}
export { EarconEditor }

