import * as P5 from 'p5';
import 'p5/lib/addons/p5.sound.min';
import 'p5/lib/addons/p5.dom.min';
import { Grid } from './Grid'
import { EarconEffect } from './../../models/Effect'
import { GridPoint } from './GridPoint';
import { MyColor } from './../MyColor';
import { Knoten } from "../knoten";
import { EarconEffectProcessor } from "../EarconEffects/EarconEffectProcessor";

export class LoadedEffect{
    startCol: number
    endCol: number
    effect: EarconEffect
    name: string;

    constructor(start: number, length: number, effect: EarconEffect){
        this.startCol = start
        this.endCol = this.startCol + length
        this.effect = effect
        this.name = "Default"
    }
}