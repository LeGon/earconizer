import * as P5 from 'p5';
import { GridPoint } from './GridPoint';
import { EarconEditor } from './EarconEditor';
import { Logger } from '../Logger/Logger';

export class Grid {

    columns: number;
    rows: number;
    height: number
    width: number
    p5: P5
    gridPoints: GridPoint[] = new Array()
    constructor(p5: P5, columns: number, rows: number, width: number, height: number) {
        if (rows % 2 == 0) {
            throw new Error("Rows muss ungerade sein!")
        }
        this.columns = columns
        this.rows = rows
        this.height = height
        this.width = width
        this.p5 = p5
        this.createPoints()
    }

    createPoints() {
        let offset = GridPoint.radius * GridPoint.stroke
        for (let j = 0; j < this.height; j += this.height / this.rows) {
            for (let i = 0; i < this.width; i += this.width / this.columns) {
                this.gridPoints.push(new GridPoint(this.p5, i + offset, j + offset))
                console.log("point created at " + i + " :: " + j)
            }
        }
    }

    getPointAt(col: number, row: number): GridPoint {
        return this.gridPoints[row * this.columns + col]
    }

    getAllPointsInColumn(col: number): GridPoint[] {
        return this.gridPoints.filter((point: GridPoint, index: number, gridPoints: GridPoint[]) => {
            return ((index) % (this.columns) == col)
        })
    }

    getColumnOfPoint(point: GridPoint): number {
        return this.gridPoints.indexOf(point) % this.columns
    }

    getLastActiveStep(start : number, end: number): number {
        let startCol = start 
        let endCol = end
        Logger.felix(startCol, "start ||end  ",  endCol)
        for (let i = endCol; i >= startCol; i--) {
            let allPointsinCol = this.getAllPointsInColumn(i)
            for(let j = 0; j < allPointsinCol.length; j++){
                if (allPointsinCol[j].active == true){
                     return i
                }
            }
           
        }
        return start - 1
    }

    findClickedCircle(mouseX: number, mouseY: number): GridPoint | undefined {
        let clickedPoint: GridPoint | undefined
        this.gridPoints.forEach((point) => {
            if (this.p5.dist(point.x, point.y, mouseX, mouseY) <= GridPoint.radius)
                clickedPoint = point
        })
        if (clickedPoint != undefined) {           
            return clickedPoint;
        }
        return undefined;
    }

    clickGridPoint(gridPoint: GridPoint){
        this.getAllPointsInColumn(this.getColumnOfPoint(gridPoint)).filter((point) => { return point != gridPoint }).forEach((point) => point.deactivate())
        gridPoint.clicked()
    }

    drawPoints() {
        this.gridPoints.forEach((point) => {
            point.draw()
        })
    }

    clear(){
        Logger.felix("Grid cleared")
        this.gridPoints.forEach((it) => it.deactivate())
    }

    drawGrid() {
        let p5 = this.p5
        const lineColor = EarconEditor.primaryColor.makeP5Color(p5)
        const lineStroke = 3
        let offset = GridPoint.radius * GridPoint.stroke
        p5.translate(offset, offset)
        p5.stroke(lineColor);
        p5.strokeWeight(lineStroke);
        let colsDrawn = 0;
        for (let i = 0; i < this.width; i += this.width / this.columns) {
            if (!(colsDrawn == 0) && !(colsDrawn == this.columns - 1))
                p5.line(i, 0, i, this.height - this.height / this.rows)
            colsDrawn += 1;
        }
        for (let j = 0; j < this.height; j += this.height / this.rows) {
            p5.line(0, j, this.width - this.width / this.columns, j)
        }
        p5.translate(-offset, -offset)
    }
}