import * as P5 from 'p5';
import { EarconEditor } from './EarconEditor';
import { MyColor } from '../MyColor';
import { Logger } from '../Logger/Logger';
export class GridPoint {

    colors: P5.Color[]

    static radius = 12
    static stroke = 2
    x: number
    y: number
    effectGroup: number = 0 //Alle Steps aus einem Effect eines sind in einer effectGroup
    locked = false
    active = false
    visible = true
    p5: P5
    color: P5.Color

    constructor(p5: P5, x: number, y: number) {
        this.p5 = p5
        this.colors = [EarconEditor.bobbelColor.makeP5Color(p5), EarconEditor.activeBobbelColor.makeP5Color(p5)]//[this.p5.color('green'), this.p5.color('red'), this.p5.color('yellow'), this.p5.color('blue')]
        this.x = x
        this.y = y
        this.color = EarconEditor.bobbelColor.makeP5Color(p5)

    }

    draw() {
        let p5 = this.p5
        const backgroundColor = EarconEditor.bobbelColor.makeP5Color(p5)
        let color = this.color
        if (this.active) {
            color = this.color
            p5.stroke(color)
            p5.strokeWeight(GridPoint.stroke)
            p5.fill(backgroundColor)
            p5.ellipse(this.x, this.y, GridPoint.radius * 2)
        }
    }

    clicked() {
        //if (!this.locked){ es wird gerade anders abgefragt obs gelockt ist ist earoneditor in der click funktion so wäre es schöner
        this.active = !this.active
        //}
    }
    deactivate() {
        this.active = false
    }

    setGroupColor() {
        if (this.locked) this.color = this.colors[0]
        else this.color = this.colors[1]
    }
}