import * as P5 from 'p5';
import 'p5/lib/addons/p5.sound.min';
import 'p5/lib/addons/p5.dom.min';
import { Knoten } from './knoten';
import { Logger } from "../scripts/Logger/Logger";
import { MyColor } from './MyColor';


export class EarconHandler{

    p5 : P5;
    firstEarcon: Knoten;
    tree : any;
    knotsize : number;
    knotlist: any; //Ein Array ähnlich des Tree Arrays, aber die Knoteninformationen wurden auf absolute Daten reduziert. Ein Knoten enthält hier die Informationen [x,y,active,color,name] um die Informationen direkt für das Zeichnen vorzuhalten. Das Array wird durch die Funktion `getKnoten` überschrieben.
    clickable : any;  
    connectionList : any; //Ein Array, welches die Anfangs und Endkoordinaten einer Knotenverbindung enthält. Der Aufbau des Array orientiert sich an dem Tree Array, aus übersichtlichkeits und debugging Gründen. die Daten werden aus Performancegründen für das Zeichnen vorgehalten. Befüllt bzw. überschrieben wird die Variable mit der Funktion `getConnections`.
    activeEarcon : Knoten | any; //Beinhaltet das derzeit ausgewählte Earcon. Die Variable wird derzeit nur in einer Funktion verwendet. Man könnte ein Entfernen aus dem Code in betracht ziehen. Für eine schnellere Abfrage des ausgewählten Earcons blieb das activeEarcon jedoch im Code bestehen.
    knotNames : any; //Eine Liste mit Knoten in deep search reihenfolge. Altlast aus der Alpha des Earconizers. (gehört weggepatcht)
    entername : boolean;
    public typeText : String;


	constructor(p5:p5 /*, Knoten*/) {
    this.p5 = p5;
	this.firstEarcon = new Knoten("New Project");
	this.tree = [[this.firstEarcon]];
	this.knotsize = 40;
	this.knotlist = [];
	this.clickable = [];
	this.connectionList = [];
	this.activeEarcon = null;
	this.knotNames = [];
	this.entername = false; 
	this.typeText = "";
	}

	getTree(){
		return this.tree;
	}
	
	getEntername()   { return this.entername;	}
	setEntername(bool: boolean)   {	this.entername = bool; }

	getTheActiveOne()   { return this.activeEarcon}
	setTheActiveOne(earcon : Knoten)   { this.activeEarcon = earcon;	}

	//Gibt ein Array mit Knoten zurück. Anordnung in DeepSearch Reihenfolge.
	getKnotenlist() {
		//Logger.toni("# getKnotenlist #");
		var knotenliste = []
		var stack = [];
		stack.push([this.tree[0][0]]);
		while (stack.length > 0 ) {
			if(stack[stack.length-1][0].hasChildren()) {
				knotenliste.push(stack[stack.length-1][0]);
				//this.clickable.push(["rect", stack[stack.length-1][0], knotenliste.length-1]);
				stack.push(stack[stack.length-1][0].getChildren());
				stack[stack.length-2].shift();
				if (stack[stack.length-2].length == 0)
					stack.splice(stack.length-2,1);
			} else {
				knotenliste.push(stack[stack.length-1][0]);
				//this.clickable.push(["rect", stack[stack.length-1][0], knotenliste.length-1]);
				stack[stack.length-1].shift();
				if (stack[stack.length-1].length == 0)
					stack.splice(stack.length-1,1);
			}
		}
		//this.clickable.push(["+", knotenliste.length]);
		this.knotNames = knotenliste;
		return knotenliste;
	} 

	//einzelnen Knoten zeichnen. Leider noch übergabe statischer Werte
	drawKnoten(x:number, y:number, active:boolean, color:MyColor, name:string) {
		//einzelnen Knoten zeichnen
		this.p5.textSize(15);
		if(active) {
			//Text
			if(this.entername==true){
				this.p5.stroke("grey");
				this.p5.fill("white");
				this.p5.rect(x-name.length*3-5, y+28, name.length*8+10, 20, 5);
			}
			this.p5.noStroke();
			this.p5.fill(196,0,68);
			this.p5.text(name, x-name.length*3, y+43);
			// Earcon
			this.p5.strokeWeight(8);
			this.p5.stroke(196,0,68);
			this.p5.fill(233,233,233);
			
		} else {
			//Text
			this.p5.noStroke();
			this.p5.fill(color.makeP5Color(this.p5));
			this.p5.text(name, x-name.length*3, y+43);
			// Earcon
			this.p5.strokeWeight(4);
			this.p5.stroke(color.makeP5Color(this.p5));
			this.p5.fill("white");

		}
		
		this.p5.ellipse(x,y,this.knotsize);
	}

	//Zeichnen der Hintergrundebenen
	drawBackground() {
		var levelwidth = 30;
		this.tree.forEach((item:any, index:number)  => {
			var i  = (index+1);
			this.p5.noStroke();
			this.p5.fill(245,245,245);
			this.p5.ellipse(50,i*100-50,levelwidth);
			this.p5.ellipse(this.p5.width-50,i*100-50,levelwidth);
			this.p5.rect(50, i*100-50-levelwidth/2,this.p5.width-100, levelwidth)
		});
	}
	
	//Zeichnet die Baumstruktur. Zuerst die Verbindungen und dann die Knoten
	drawTree() {
		//Logger.toni("knotlist: " , this.knotlist);
		this.knotlist.forEach((item:any, index:number, array:any) => { //Level
			item.forEach((jitem:any, jindex:number, array:any) => { //Knoten
				// Die Connections des Knotens malen
				this.p5.stroke(62,62,62);
				this.p5.strokeWeight(4);
				if(index < this.connectionList.length){
					if(jindex < this.connectionList[index].length){
						this.connectionList[index][jindex].forEach((citem:any , cindex:number, array:any) => {
							this.p5.stroke(62,62,62);
							this.p5.strokeWeight(4);
							this.p5.noFill();
							this.p5.line(this.connectionList[index][jindex][cindex][0],
								this.connectionList[index][jindex][cindex][1]+50,
								this.connectionList[index][jindex][cindex][0],
								this.connectionList[index][jindex][cindex][1]+65);
							this.p5.bezier(this.connectionList[index][jindex][cindex][0],
								this.connectionList[index][jindex][cindex][1]+65,
								this.connectionList[index][jindex][cindex][2],
								this.connectionList[index][jindex][cindex][3]-35,
								this.connectionList[index][jindex][cindex][2],
								this.connectionList[index][jindex][cindex][3]-35,
								this.connectionList[index][jindex][cindex][2],
								this.connectionList[index][jindex][cindex][3]);
							if(this.connectionList[index][jindex][cindex][4]){
								this.p5.fill("white");
								this.p5.strokeWeight(2);
								this.p5.rect(this.connectionList[index][jindex][cindex][0]-10,
									this.connectionList[index][jindex][cindex][1]+55, 20, 20, 3);
								
								this.p5.stroke(13,166,135);
								this.p5.line(this.connectionList[index][jindex][cindex][0]-2,
									this.connectionList[index][jindex][cindex][1]+60,
									this.connectionList[index][jindex][cindex][0]-2,
									this.connectionList[index][jindex][cindex][1]+70);
								this.p5.line(this.connectionList[index][jindex][cindex][0]+2,
									this.connectionList[index][jindex][cindex][1]+60,
									this.connectionList[index][jindex][cindex][0]+2,
									this.connectionList[index][jindex][cindex][1]+70);
							}
						});
					}
				}
				
				//Die Knoten malen
				if(jitem[2]){
					this.drawAddButton(index+2);
					this.drawDeleteButton(index+1);
				}
				this.drawKnoten(jitem[0],jitem[1], jitem[2], jitem[3],jitem[4]);
				
			});
		});
	}

	//Zeichnet den + Button zum Hinzufügen von Knoten
	drawAddButton(level:number){
		var levelwidth = 30;
		this.p5.stroke("white");
		this.p5.strokeWeight(4);
		this.p5.fill("white");
		this.p5.ellipse(this.p5.width-50,level*100-50,levelwidth+5);
		this.p5.textSize(20);
		this.p5.noStroke();
		this.p5.fill("gray");
		this.p5.text("+", this.p5.width-55,level*100-44);
		
		//this.clickable.splice(this.clickable.length-2, 1);
		this.clickable.push(["+", this.p5.width-50, level*100-50, levelwidth+5]);
	}

	//Zeichnet den - Button zum Entfernen von Knoten oder Teilpfaden
	drawDeleteButton(level:number){
		var levelwidth = 30;
		this.p5.stroke("white");
		this.p5.strokeWeight(4);
		this.p5.fill("white");
		this.p5.ellipse(this.p5.width-50,level*100-50,levelwidth+5);
		this.p5.textSize(30);
		this.p5.fill("gray");
		this.p5.noStroke();
		this.p5.text("-", this.p5.width-55,level*100-44);
		
		//this.clickable.splice(this.clickable.length-2, 1);
		this.clickable.push(["-", this.p5.width-50, level*100-50, levelwidth+5]);
	}

	//Gibt die Anzahl der Blattknoten unter dem aktuellen Knoten an. Hilfsklasse für optimierte Anordnung. Bisher keine Verwendung
	getMaxLeafCount(knoten:Knoten){
		var i = knoten.getAncestors().length;
		var maxLeafCount = 0;
		var leafCount = 0;
		for( i+1 ; i < this.tree.length ; i++){
			leafCount = 0;
			for(var j=0; j<this.tree[i].length;j++){
				if(this.tree[i][j].hasAncestor(knoten))
					leafCount++;
			}
			if(leafCount > maxLeafCount)
				maxLeafCount = leafCount;
		}
		return maxLeafCount;
	}
	
	//Iteriert über die Baumstruktur und aktualisiert die x/y Werte für jeden Knoten
	reorderTree(){
		var mode = "auto";
		var abstand;
		var longestLevel = 0;
		var firstLevelAbstand: number[] = [];
		//Position eines Knotens im Objekt aktualisieren entsprechend des Tree-Arrays
		if(mode == "opt"){
			/*leider noch nicht fertig
			Es soll von oben nach unten der Platz aufgeteilt werden. Dabei wird vertikal
			geschaut, was die maximale anzahl an blättern ist um nicht zu wenig platz zu vergeben*/
			for(var i=0; i<this.tree.length; i++) {
				if (this.tree[i].length > longestLevel)
					longestLevel = i;
			}
			for(var i=0; i< this.tree[1].length; i++){
				firstLevelAbstand.push(0);
				this.tree[longestLevel].forEach((element:  any) => {
					if(element.hasAncestor(this.tree[1][i]))
						firstLevelAbstand[i]++;
				});
			}
			Logger.toni("lvl 1 Array:  " + firstLevelAbstand);
				
			var bla = [[this.tree[0][0],(this.p5.width-50)]]; //speichert die parents und deren platz
			this.tree[0][0].setx((this.p5.width-50)/2);
			this.tree[0][0].sety(100-50);
			for(var i=1; i<this.tree.length; i++){
				var bli = []; //speichert die parents und deren platz (temp)
				var blu = 0; // speichert den gesamten maxLeaf Platzbedarf dieser Ebene
				abstand=0;
				for(var j=0; j<this.tree[i].length; j++){
					var maxleaf = this.getMaxLeafCount(this.tree[i][j]);
					if(maxleaf == 0)
						maxleaf = 1; 
					blu = blu + maxleaf;
					Logger.toni("blu " + blu);
				}
				for(var j=0; j<this.tree[i].length; j++){
					for(var k=0; k<bla.length; k++){
						Logger.toni(bla);
						if(bla[k][0].hasChild(this.tree[i][j])){
							var maxleaf = this.getMaxLeafCount(this.tree[i][j]);
							if(maxleaf == 0)
								maxleaf = 1; // knoten braucht platz, auch, wenn er keine blätter hat
							bli.push([ this.tree[i][j] , ((bla[k][1])/blu)*maxleaf ] );
						}
					}
					
					Logger.toni("abstand " + abstand);
					this.tree[i][j].setx(abstand + (bli[bli.length-1][1]/2));
					this.tree[i][j].sety((i+1)*100-50);
					abstand = abstand + bli[bli.length-1][1];
				}
				bla = bli;
			}
	}

		if(mode =="auto"){
			for(var i=0; i<this.tree.length; i++){
				abstand = (this.p5.width-100)/(this.tree[i].length+1);
				for(var j=0; j<this.tree[i].length; j++){
					this.tree[i][j].setx(abstand*(j+1));
					this.tree[i][j].sety((i+1)*100-50);
				}
			}
		}
	}
	
	//Hilfsfunktion zum Erstellen von Demodaten. Kann später zum laden von Projektdateien oder Templates genutzt werden
	boot(){
		// Hilfsklasse zum erstellen von Demodaten
		// Kann später zum laden von Jsons genutzt werden.
		Logger.toni("=========>boot()");
		/*if(this.firstEarcon.hasChildren()){

			Hier müsste das aktuelle Projet gespeichert werden

		}else{*/
			this.firstEarcon = new Knoten("Messenger");
			this.tree = [[this.firstEarcon]];
			this.firstEarcon.earconEffect.steps = [true];
			this.firstEarcon.earconEffect.pitches = [0];

			
			this.firstEarcon.addChild("Message");
			var meh = this.firstEarcon.getchild(0);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [0];
			meh.addChild("Direct");
			meh.addChild("Group");

			var muh = meh.getchild(0);
			muh.setSymmetric(true);
			muh.earconEffect.steps = [true];
			muh.earconEffect.pitches = [1];
			meh = meh.getchild(1);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [2];
			
			muh.addChild("send");
			muh.addChild("receive");
			meh = muh.getchild(0);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [-1];
			muh = muh.getchild(1);
			muh.setSymmetric(true);
			muh.earconEffect.steps = [true];
			muh.earconEffect.pitches = [3];

			muh = this.firstEarcon.getchild(0).getchild(1);
			muh.addChild("send");
			muh.addChild("receive");
			meh = muh.getchild(0);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [1];
			muh = muh.getchild(1);
			muh.setSymmetric(true);
			muh.earconEffect.steps = [true];
			muh.earconEffect.pitches = [3];

			this.firstEarcon.addChild("Call");
			meh = this.firstEarcon.getchild(1);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [false, true];
			meh.earconEffect.pitches = [0,0];
			meh.addChild("Call in");
			meh.addChild("Call out");

			muh = meh.getchild(0);
			muh.setSymmetric(true);
			muh.earconEffect.steps = [true];
			muh.earconEffect.pitches = [-2];
			meh = meh.getchild(1);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [2];
			
			muh.addChild("accept");
			muh.addChild("decline");
			meh = muh.getchild(0);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [-3];
			muh = muh.getchild(1);
			muh.setSymmetric(true);
			muh.earconEffect.steps = [true];
			muh.earconEffect.pitches = [-1];

			muh = this.firstEarcon.getchild(1).getchild(1);
			muh.addChild("start");
			muh.addChild("cancel");
			meh = muh.getchild(0);
			meh.setSymmetric(true);
			meh.earconEffect.steps = [true];
			meh.earconEffect.pitches = [1];
			muh = muh.getchild(1);
			muh.setSymmetric(true);
			muh.earconEffect.steps = [true];
			muh.earconEffect.pitches = [3];

			//this.firstEarcon.setActive();
			
		//}
			Logger.toni("<=========boot()");
	}
	
	//aktualisiert das tree-Array und damit die Baumstruktur
	updateTree(){ 
	/*Aufbau eines Tree Arrays. Braucht eine [i][j] Liste mit dem ersten
	Knoten. also Quasi 
			var ersterKnoten = new Knoten("erster");
			updateTree([[ersterKnoten]]);    */
		if (this.tree[0][0].getChildren().length == 0){
			this.tree = [[this.firstEarcon]];
			this.reorderTree();
			this.getKnoten();
			this.getConnections();
			this.getKnotenlist();
			return;
		}

		var newline = [this.firstEarcon];
		this.tree = [];
		this.tree.push(newline);
		newline = newline[0].getChildren();
		this.tree.push(newline);
		for (var i=1; i< this.tree.length; i++){
			for(var j=0;j<this.tree[i].length;j++){
				newline = this.tree[i][j].getChildren();
				if(newline.length != 0){
					if (this.tree.length <= i+1)
					this.tree.push(newline);
					else{
						newline.forEach((item:any, index:number, array:any) => {
							this.tree[i+1].push(item);
						});
					}
				}
			}
		}
		if (this.tree[this.tree.length-1].length == 0)
		this.tree.pop();

		this.reorderTree();
		this.getKnoten();
        this.getConnections();
        this.getKnotenlist();
		return this.tree;
	}
	
	getKnoten(){
		/*  Code um Tree in eine Liste von Knoten-Koordinaten
		umzuwandeln. Um CPU zu schonen und den cache nicht jedes mal vollzumüllen */
		var knoten:any = [];
		this.tree.forEach((item:any, index:number, array:any) =>{ //Generation/ Level
			knoten.push([]);
			item.forEach((jitem:any, jindex:number, array:any) => { //Knoten
				knoten[index].push([jitem.getx(), jitem.gety(), jitem.isActive(), jitem.getColor(), jitem.getName()]);
				this.clickable.push(["circle", jitem ,jitem.getx(), jitem.gety()]);
			});
		});
		this.knotlist = knoten;
		
		return knoten;
	}
	
	getConnections(){
		/*code um den tree in eine Tree-Array mit Koordinaten umzuwandeln
		um knoten per linie verbinden zu können.*/
		var verbindungen:any = [];
		var children = [];
		this.tree.forEach((item:any, index:number, array:any) => { //Generation/ Level
			verbindungen.push([]);
			item.forEach((jitem:any, jindex:number, array:any) => { //Knoten
				if (jitem.getChildren().length == 0)
					{return;}
				children = jitem.getChildren();
				verbindungen[index].push([]);
				children.forEach((citem:any, cindex:number, array:any) => { //child
					verbindungen[index][verbindungen[index].length-1].push([jitem.getx(),jitem.gety(),citem.getx(),citem.gety(),jitem.isSymmetric()]);
				});
			});
			if (verbindungen[verbindungen.length-1].length == 0)
				verbindungen.pop();
		});
		
		this.connectionList = verbindungen;
		return verbindungen;
	}

	//gibt das Objekt aus dem clickable-Array wieder, falls dieses mit der Maus getroffen wurde
	whatchyaClickin(MouseX:number,MouseY:number) {
		//Logger.toni("# whatchyaClickin #", this.clickable);
		var target = this.getTheActiveOne();
		
		for(var i = 0; i < this.clickable.length; i++) {
			var item = this.clickable[i];
		//this.clickable.forEach((item:any, index:number, array:any) => {
			if ( MouseX > 0 && MouseY > 0) {
				
				this.entername == false;
				if(item[0] == "circle"){
					if(this.p5.dist(MouseX,MouseY,item[2],item[3]) <= this.knotsize) {
						target = item[1];
						//return item[1];
						break;
					}
				}
				if (item[0] == "+") {
					if(this.p5.dist(MouseX,MouseY,item[1],item[2]) <= item[3]) {
						Logger.toni("+ wirklich getroffen");
							if(this.activeEarcon == null){}
							else{
								this.activeEarcon.addChild("");
								target = this.activeEarcon.getchild(this.activeEarcon.getChildren().length-1);
								this.typeText = "";
								this.entername = true;
								//return target;
								break;
							}
					}	
				}
				if (item[0] == "-") {
					if(this.p5.dist(MouseX,MouseY,item[1],item[2]) <= item[3]) {
							if(this.activeEarcon == null){}
							else {
								if(this.activeEarcon.getDirectParent() == false){
									this.firstEarcon = new Knoten("new Project");
									this.firstEarcon.setActive();
									target = this.firstEarcon;
								}
								else{
									target = this.activeEarcon.getDirectParent();
									this.activeEarcon.getDirectParent().deleteChild(this.activeEarcon);
									//return target;
									break;
								}
							}
					}	
				}	
			} else {
					this.entername == false;
					target = null;
			}
		}
		this.knotNames.forEach((knoten:Knoten)=>{
			knoten.setInactive();
		});
		this.clickable = [];
		//Logger.toni(" taget = ", target);
		return target;
	}
	
	
}
