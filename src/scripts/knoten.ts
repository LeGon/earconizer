import { EarconEffect } from "../models/Effect";
import { Logger } from "../scripts/Logger/Logger";
import { MyColor } from "./MyColor";

export class Knoten{

    name : String;
    x : number;
    y : number;
    children : any;
    active = false;
    color : MyColor;
    ancestors : Knoten[];
    earconEffect: EarconEffect
    symmetric: boolean; // ein Flag was für symmetrische Earcons angedacht ist. Derzeit hat es keine eigene Logik und wird lediglich für optische Zwecke verwendet

    constructor(name: string) {
      this.name = name;
      this.x = 100;
      this.y = 100;
      //this.loop = 1;
      //this.pitch = 0;
      this.children = [];
      this.active = false;
      this.color = new MyColor(62);
      this.ancestors = [];
      this.earconEffect = new EarconEffect();
      this.symmetric = false;
    }
    
      //Dem Knoten ein Child-Koten hinzufügen
    addChild(name:string){
        if(name == this.name){
            name = name + " new";
            var knoten = new Knoten(name);
        } else 
            var knoten = new Knoten(name);
        this.ancestors.forEach((item: any) => {
            knoten.addOrigin(item);
        });
        knoten.addOrigin(this);
        this.children.push(knoten);
        Logger.toni(this.name, " wurde ", name," hinzugefügt.");
    }

    
      
      //gibt zurück, ob der Knoten mindestens 1 Kind hat
    hasChildren() {
          if(this.children.length == 0)
              return false;
          return true;
    }

    //gibt true zurück, wenn das children-Attribut nicht leer ist
    hasChild(knoten:Knoten){
        for(var i=0;i < this.children.length; i++){
            if(this.children[i]==knoten)
                return true;
        }
        return false;
    }
    
    //gibt true zurück, wenn der angegebene Knoten ein direkter Kind-Knoten ist
    getAllToRoot(): Knoten[]{
        var parentKnoten: Knoten[] = []
        parentKnoten.push(this);
        var actKnoten:Knoten = this
        while(actKnoten.hasAncestor){
            actKnoten = actKnoten.ancestors[0]
            parentKnoten.push(actKnoten);
        }
        return parentKnoten
    }

    //Gibt einen bestimmten Child-Koten zurück
    getchild(index: number){
        Logger.toni("getchild(index): ");
        Logger.toni(this.children);
        if(this.children.length > 0 ) {
            return this.children[index];
        }
        return null;
    }
      
      //Gibt ein Array mit Knoten zurück
    getChildren(){
          var kinners :any = [];
          this.children.forEach(function(item:any, index: number, array:any) {
              kinners.push(item);
          });
          return kinners;
      }

    //löscht einen angegebenen Knoten aus dem children-Array, falls vorhanden
    deleteChild(knoten:Knoten){
        this.children.splice(this.children.indexOf(knoten),1);
    }
    
    //fügt einen Knoten zum Ancestor-Array hinzu
    addOrigin(ancestor: Knoten){
        this.ancestors.push(ancestor);
    }

    //gibt das Ancestors-Array inklusive aktuellem Knoten (this) zurück. (Nochmal auf Sinnhaftigkeit zu prüfen. Vermutlich überflüssig)
    getPath() {
        var path :Knoten[] = [];
        this.getAncestors().forEach((item: Knoten) => {
            path.push(item);
        });
        path.push(this);
        return path;
    }

    //Gibt den direkt übergeordneten Knoten zurück
    getDirectParent(){
        if(this.ancestors.length >0)
            return this.ancestors[this.ancestors.length-1];
        else
            return false;
    }

    //gibt das ancestos-Array zurück
    getAncestors(){
        return this.ancestors;
    }

    //gibt true zurück, falls gesuchter Knoten im übergeordneten Pfad vorhanden
    hasAncestor(knoten:Knoten){
        for(var i=0;i < this.ancestors.length; i++){
            if(this.ancestors[i]==knoten)
                return true;
        }
        return false;
    }
  
      getx(){return this.x;} //gibt x-Wert im Canvas zurück
      gety(){return this.y;} //gibt y-Wert im Canvas zurück
        
      setx(x:number){this.x = x;} //setzt x-Wert im Canvas
      sety(y:number){this.y = y;} //setzt y-Wert im Canvas
      
      getName() {return this.name;}
      setName(name:string){this.name = name;}
      
      //Setzt den Knoten auf Aktiv (ausgewählt)
      setActive() {this.active = true;}
      //Setzt den Knoten auf Inaktiv (nicht ausgewählt)
      setInactive() {this.active = false;}
      //Gibt zurück, ob ein Knoten aktiv ist bzw. ausgewählt/angeklickt wurde
      isActive() {
          if (this.active)
              return true;
          else	
              return false;
      }

      isSymmetric(){  return this.symmetric;   }

      setSymmetric(value:boolean){ this.symmetric = value;  }
      
      //gibt die Farbe eines Knotens zurück
      getColor() {return this.color;}
      //Ändere die Farbe des Knotens
      setColor(color:MyColor) {this.color = color;}
    
  }