import * as Tone from "tone";
import { Logger } from "../Logger/Logger";

export class PlayerControll {
    play: boolean;
    bpm: number;
    player: Tone.Player;
    constructor(play: boolean, bpm: number){
        this.play = play;
        this.bpm = bpm;
        Tone.Transport.bpm.value = this.bpm;
        Logger.max("Player erzeugt");
        this.player = new Tone.Player("./tone1.mp3").chain(Tone.Master); //Hier könnte Looper mit rein
        this.player.autostart = play;
        Logger.max("Default BPM:", Tone.Transport.bpm.value); //zeigt BPM änderung
    }

    stop(){
        //this.player.stop();
    }

    start(){
        //this.player.start();
    }

    bpmNeu(bpmNeu: number){
        Tone.Transport.bpm.value =  bpmNeu;
        Logger.max("Aktuelle BPM:", Tone.Transport.bpm.value)
    }

}