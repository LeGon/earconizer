import * as P5 from 'p5';

export class MyColor {
    private _r: number = 0;
    private _g:number = 0;
    private _b: number = 0;
    private _alpha = 1
    constructor(r:number, g?:number, b?:number, a?:number){
        this.r = r;
        this.b = b || r;
        this.g = g || r;
        this.alpha = a || 1
    }

    set r(r: number){
        if (this.isInRange(r)) this._r = r
    }
    set b(b: number){
        if (this.isInRange(b)) this._b = b
    }
    set g(g: number){
        if (this.isInRange(g)) this._g = g
    }
    set alpha(alpha: number){
        if (this.isInRange(alpha), 0, 1) this._alpha = alpha
    }

    isInRange(input: number, start?: number, stop?: number): Boolean{
        start = start || 0
        stop = stop || 255
        if (input< start || input > stop) return false
        else return true
    }

    makeP5Color(p5: P5): p5.Color{
        return p5.color('rgba(' + this._r + ',' + this._g + ',' + this._b  + ',' +  this._alpha + ')')
    }
}