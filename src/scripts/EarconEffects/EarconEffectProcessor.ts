import * as Tone from 'tone';
import { EarconEffect } from './../../models/Effect'
import { Knoten } from './../knoten';
import { Logger } from '../Logger/Logger';




export class EarconEffectProcessor {
    maxSequenceSteps: number
    sequenceLoop: Tone.Loop
    effects: EarconEffect[]
    chainedSteps: boolean[]
    chainedPitches: number[]
    stepCounter: number = 0
    player: Tone.Player | undefined
    pitchShift: Tone.PitchShift
    stopFunction: () => any
    startFunction: () => any

    constructor(maxSequenceSteps: number, stopFunction: () => any, startFunction: () => any) {
        this.maxSequenceSteps = maxSequenceSteps - 1
        //this.sequenceLoop = new Tone.Loop(this.loopFunction.bind(this), "1n")
        this.sequenceLoop = new Tone.Loop(this.loopFunction.bind(this), '4n')
       
       // this.sequenceLoop.iterations = 0
        this.pitchShift = new Tone.PitchShift()
        this.pitchShift.toMaster()
        this.effects = []
        this.chainedPitches = []
        this.chainedSteps = []

        this.stopFunction = stopFunction
        this.startFunction =startFunction
        Tone.Transport.bpm.value = 120
        Tone.Transport.loop = false
    }

    loadSound(path: string) {
        Logger.felix("entering loadSOund")
        this.stopPlay()
        if (this.player != undefined) {
            this.player.load(path)
        }
        else {
            this.player = new Tone.Player(path)
        }
       this.player.connect(this.pitchShift)
    }

    startPlay() {
        Logger.felix("Playing effect ", JSON.stringify(this.chainedSteps), JSON.stringify(this.chainedPitches))
        this.sequenceLoop.start(0)
        this.sequenceLoop.cancel()
        this.sequenceLoop = new Tone.Loop(this.loopFunction.bind(this), "4n").start(0)
        Tone.Transport.start()
    }

    pausePlay() {
        Tone.Transport.stop()
    }

    stopPlay() {
        this.pausePlay()
        this.stepCounter = 0
    }

    loadEarcon(effects: EarconEffect[]) {
        Logger.max("entering loadEarcon")
        this.stopPlay()
        this.effects.length = 0//resetttet die arrays
        this.chainedSteps.length = 0
        this.chainedPitches.length = 0
        this.effects = effects // effecte werden kopiert
        this.chainEffects() //die steps und pitches werden in die arrays gepackt
        //this.sequenceLoop.iterations = this.chainedSteps.length //die iterations werden auf die anzahl der steps gesetzt
        this.startPlay();
        this.startFunction();
        Logger.max("effects loaded ", JSON.stringify(this.effects))
    }

    private chainEffects() {
        this.effects.forEach((it) => {
            it.steps.forEach((step) => this.chainedSteps.push(step))
            it.pitches.forEach((pitch) => this.chainedPitches.push(pitch))
        })
    }

    private loopFunction(time: Tone.Encoding.Time) {
        Logger.felix("looop me ", this.stepCounter)
       
        if (this.chainedSteps[this.stepCounter] && this.player != undefined) {
            Logger.felix("pitch ", this.chainedPitches[this.stepCounter])
            this.pitchShift.pitch = this.chainedPitches[this.stepCounter] * -4
            this.player.start()
        }
        this.stepCounter++
        if (this.stepCounter == this.chainedSteps.length) {
            //this.stepCounter = 0
            this.stopPlay()
            this.stopFunction()

            
         }
    
    }


}