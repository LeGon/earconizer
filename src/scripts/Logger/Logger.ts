export class Logger{
    static showFelix = true
    static showToni = false
    static showMax = false

    static felix (...rest: any[]){
        if (this.showFelix)
        console.log(rest)
    }

    static toni (...rest: any[]){
        if (this.showToni)
        console.log(rest)
    }

    static max (...rest: any[]){
        if (this.showMax)
        console.log(rest)
    }
}