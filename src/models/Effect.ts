export class EarconEffect {
    steps: boolean[]
    pitches: number[]// pitch per loop


    constructor(steps?: boolean[], pitches?: Number[]) {
        this.steps = steps || new Array()
        this.pitches = pitches || new Array()
    
    }

    length(){
        return this.steps.length
    }

    cutIfTooLong(maxSize: number){ //nur benötigt wenn man mehrer effekt kombiniert
        if(this.steps.length >= maxSize){
            this.steps.slice(this.steps.length - maxSize, this.steps.length)
            this.pitches.slice(this.pitches.length - maxSize, this.pitches.length)
        }
    }
/*
    calcEffectLength(): number{
        if (this.steps.length > 0) {
            let effectLength = this.steps.length - 1
            if (this.steps[effectLength] == false) {
                while (this.steps[effectLength - 1] == false) {
                    effectLength--;
                }
            }
            return effectLength
        }
        else return 0;
    }
    */
}
