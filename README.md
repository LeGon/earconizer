# Earconizer v1.1

### Setup
---
Clone the repo and open powershell. Update your yarn version:   
`yarn`

Then build the project using:   
`yarn build`

You'll also have to start the server for audio output:   
`yarn run dev`

A new browser tab/window should appear with the Earconizer displayed. Have fun!

webpack is set to hotbuild -> filechange invokes immediately rebuilding

### Used programs and frameworks to develop and test the earconizer:
---
- visual studio Code 1.37.1
- Mozilla Firefox 68.0.2 
- yarn 1.15.2
- p5 0.7.3
- tone.js 13.4.9
- vue.js 2.5.2
- fontawesome-free 5.6.3
- lite-server 2.5.4
- ts-loader 4.4.2
- typescript 2.8.4
- webpack 4.14.0
- webpack-cli 3.1.2


### How to use
---
<img src="src/pictures/example_numbers.png" alt="example" style="float:right;" />

The first Earcon serves as the start node (1). 
By clicking on an earcon, it can be marked (2). Click again on an activated Earcon to rename it.
For a marked Earcon the options + and - are available on the left side (3). By clicking on the minus symbol, the Earcon (including subordinated Earcons) can be deleted. By clicking on the plus symbol a new Earcon will be added below the previously selected one.

If an Earcon is selected, a tone sequence can be created and adjusted in the Editor. Each Earcon inherits the tone sequence of its predecessor. Sound sequences of the previous Earcons can only be edited if the corresponding Earcons are selected.

Tones are arranged in a grid and therefore have fixed frequencies. By clicking on a gridpoint a sound can be added(4). Clicking on a sound again will remove it.

The currently selected Earcon can be played again using the Play button(5). The slider on the left serves for to set the BPM (6).
Earcon templates can be imported from the Template Browser (7) into the project window.

### Structure of vue components:
---

<img src="src/pictures/vue-components.png" alt="Vue Components" style="float:right;" />

**App.vue:** Container for Browser.vue and Editor.vue to split the layout into two columns.   
**Browser.vue:** Includes Transport.vue and BTemp.Vue   
**Transport.vue:** Component for audio player and BPM slider   
**BTemps.vue:** Entries for templates in a hierarchical structure   
**Editor.vue:** Container for EarconEditor and project structure.   
**EarconEditorComp.vue:** Displays the information of an earcon and its ancestors up to root. Arrangement of certain pitch values in a grid. Value editing only for Earcon/Nodes selected in Implement.vue possible.   
**Implement.vue:** Basis for the project, which always consists of at least one root-earcon. Root-Earcon = Project name. Add, rename and delete Earcons/ Earcon paths.   


### Communication between the components
---

An event bus is used for the communication between the components. The initialization is done via EventBus.vue.
To trigger an event the following command is necessary:
	
	var event = this.$bus.$emit("EventName", params);
The EventBus is handled with the following command:

	var event = this.$bus.$on("EventName", function(params: any){}
	
The Vue components were developed separately with their respective classes, which makes the following overview a useful representation of the used events. You can see under which component an event is triggered and where this event is retrieved.

<img src="src/pictures/EventBus.png" alt="Event Bus" style="float:right;" />

### Own classes

---

The source code is mostly annotated, which is why only a rough overview of the created classes is given here.

__Knoten__ 

A node or earcon is defined by this class. 
The x- and y-value determines the position in the canvas. The values are overwritten by the function `EarconHandler.reorderTree`. 
`children` contains an array with all directly subordinated nodes while `ancestors` contains an array of nodes that are parent to the current node. The root node has the index 0.
`active` indicates whether the node is currently selected to provide a different appearance of nodes.
The attribute `EarconEffect` contains the auditory properties of the node. (See EarconEffect class)
You can generate a tree structure with the children Atrribut using a root earcon and have it drawn using p5. This task is handled by the class EarconHandler.

__EarconHandler__

This class is used to manage Earcons in a tree structure. 
Starting from the root node (`firstEarcon`) this tree structure is stored in the variable `tree`. This is a multidimensional array. The first dimension specifies the levels/generations of the tree structure. The nodes are located in the second dimension.
The information for the nodes and the connections were prepared in extra variables for drawing (`connectionList, knotlist`). The `clickable` array contains all elements for which interaction is possible. Since different elements with different functions are scheduled on a canvas, this variable was sufficient as a helpful interim solution. The node selected by the user can be retrieved via the `activeEarcon` variable. To rename a node the `entername` variable is set after a new selection and allows input via keyboard. The value for the new node name is then set via `typeText`.

__EarconEffectProcessor__

This class uses tone.js and is responsible for audio playback. This is where the audio chain and loop are created. This class holds the playback function of the final loop and is the reference point for the audio related event triggers.

	
__EarconEffect__

Contains information about the effects of a node. The size of the `steps` array indicates how many grid points belong to each earcon. False values define a pause or a gap. `pitches` indicates the vertical value of a point on the grid. The middle horzontal line on the grid is defined by 0. The value is increased by 1 with each grid step to the top and reduced by 1 to the bottom.

__EarconEditor__

In this class the Earcon information from the tree structure and the Grid information are combined. 
The `loadKnoten` function loads the Earcon selected in the Implement component. This means that all nodes on the path are queried beginning from the root node. The effects of each node are loaded and cumulated. Then the effect information is passed on to the `EarconEffectProcessor`.

__Grid__

The grid is displayed in the upper part of the Earconizer. This class is used to manage the created GridPoints and to draw the Grid and all Gridpoints. The number of rows of a grid must be odd (Check EarconEffect).
	
__GridPoint__

This class defines a point in the grid. 

__LoadedEffect, MyColor__

Data classes. simple to understand and require no explanation.
